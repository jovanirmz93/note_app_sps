'use strict'


async function getValidUser(id) {

  const functionName = 'ValidateGetUser';
  const roles = await CONN('user').select('id').where({ id, isActive: true });

  if (!roles.length) log(functionName, `user selected not exist`);

  return roles[0];
}

async function validateUserData(rolData, privateId = undefined) {
  const functionName = 'validateSaveUser';

  if (!rolData.name) {
    log(functionName, `Complete all data`);
  }

  const rol = !privateId ? await CONN('user').select('privateId').where({ code: rolData.name, isActive: true })
    : await CONN('user').select('privateId').whereNot({ privateId }).andWhere({ code: rolData.name, isActive: true });

  if (rol.length) log(functionName, `user code already exist`);
}

module.exports = {
  getValidUser,
  validateUserData
}