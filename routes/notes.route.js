'use strict'

const express = require('express');

const Users = require('../controllers/users.controller');

const api = express.Router();

api.get('/getValidUser',  Rol.getRol);
api.post('/validateUserData', Rol.saveRol);

module.exports = api;