'use strict'

const rolValidator = require('../validators/userValidator');


/* -------------------------------------------------------------------------- */
/*                                   getRol                                   */
/* -------------------------------------------------------------------------- */

async function getUser (req, res) {
  const functionName = 'getUser';
  
  try {
    const id = Number(req.params.id);
    const user = await userValidator.getValidUser(id);
    delete user.privateId;

    return res.send({ succes: true, user });
  } catch (error) {
    error = log(functionName, error, true);
    return res.send({ error: 'error', message: 'not found' });
  }
}

/* -------------------------------------------------------------------------- */
/*                               saveRol                                      */
/* -------------------------------------------------------------------------- */

async function saveUser(req, res){
  const functionName = 'saveUser';
  let transaction = null;

  try{
    const user = {
      name: req.body.name
    };

    await userValidator.ValidateUserData(user);
    transaction = await CONN.transaction();
    const id = await common.getNextIndex('user', {}, transaction);
    const result = await transaction('user').insert({ ...rol, id });

    if (!result.length) log.logError(functionName, `Rol not saved`);

    await transaction.commit();

    return res.send({success: true, id});
  }catch(error){
    if(transaction) await transaction.rollback();
    error = log(functionName, error, true);
    return res.send({error: 'error', message: 'not found'})
  }
}




module.exports = {
  getRol,
  saveRol
};
