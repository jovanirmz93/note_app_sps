'use strict'

const express = require('express')
const app = express(); 
// let app = require('./app');
let port = 3000;

app.listen(port, function () {
	console.log(`Aplicacion backend_notesApp funcionando en el puerto http://localhost:${port}`);
});